import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sage/login.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:io';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  if (defaultTargetPlatform == TargetPlatform.android) {
    await Firebase.initializeApp();
  } else {
    await Firebase.initializeApp(
      options: const FirebaseOptions(
        apiKey: "AIzaSyAXJgAKXlP4nJHBBhaR8jD444RG_rVa5rI",
        projectId: "syge-d7c1a",
        messagingSenderId: "1094265846364",
        appId: "1:1094265846364:web:1441d75087c3acd21a0499",
      ),
    );
  }

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.black,
      ),
      home: Login(),
      // home: TestFirebase(),
      // home: GetUserName('Adler@teachers.rantobeng.sage.com'),
    );
  }
}

class TestFirebase extends StatelessWidget {
  // const TestFirebase({ Key? key }) : super(key: key);
  final Stream<QuerySnapshot> teachers =
      FirebaseFirestore.instance.collection('TeachersInfo').snapshots();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Test Firebase'),
      ),
      body: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Reading data from teachers',
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            Container(
                height: 250,
                padding: EdgeInsets.symmetric(vertical: 20),
                child: StreamBuilder<QuerySnapshot>(
                  stream: teachers,
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return Text('Something went wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Text('Loading..');
                    }

                    final data = snapshot.requireData;

                    return ListView.builder(
                      itemCount: data.size,
                      itemBuilder: (context, index) {
                        return Text('The data is ${data.docs[index]['Name']}');
                      },
                    );
                  },
                )),
          ],
        ),
      ),
    );
  }
}
