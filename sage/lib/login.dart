import 'package:flutter/material.dart';
import 'Teachers/appTeachers.dart';
import 'dart:io';
import 'Teachers/teacherDS.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final double _width = 350;
  // final IdStorage storage = IdStorage();

  @override
  Widget build(BuildContext context) {
    var useNameController = TextEditingController();
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'LogIn',
        ),
      ),
      body: Padding(
          padding: const EdgeInsets.all(25),
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                  width: _width,
                  child: TextFormField(
                    controller: useNameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.zero)),
                      hintText: 'Username',
                    ),
                  )),
              Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: SizedBox(
                    width: _width,
                    child: TextFormField(
                      restorationId: 'password_text_field',
                      obscureText: true,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.zero)),
                        hintText: 'Password',
                      ),
                    ),
                  )),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (BuildContext context) {
                      // widget.storage.writeID(useNameController.text);
                      return AppTeacher(useNameController.text);
                      // return writingID(useNameController.text);
                    }));
                  },
                  child: const Text('Sign In'),
                ),
              ),
            ],
          ))),
    );
  }
}

// class writingID extends StatefulWidget {
//   final storage = IdStorage();
//   String ID;
//   writingID(this.ID);

//   @override
//   _writingIDState createState() => _writingIDState(ID);
// }

// class _writingIDState extends State<writingID> {
//   String ID;
//   _writingIDState(this.ID);

//   @override
//   void initState() {
//     super.initState();
//     widget.storage.writeID(ID);
//     print('login: $ID');
//   }

//   @override
//   Widget build(BuildContext context) {
//     return AppTeacher(ID);
//   }
// }
