import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Data extends StatefulWidget {
  const Data({Key? key}) : super(key: key);

  @override
  State<Data> createState() => _DataState();
}

class _DataState extends State<Data> {
  @override
  Widget build(BuildContext context) {
    double width_size = MediaQuery.of(context).size.width;
    double height_size = MediaQuery.of(context).size.height;
    final kPrimaryColor = Colors.black;

    final Stream<QuerySnapshot> learners_marks = FirebaseFirestore.instance
        .collection('Teacher_Info')
        .where('Teacher_Number', isEqualTo: 'T8')
        //.where('Subject_Name', isEqualTo: Subject_input)
        .snapshots();

    return Container(
      //  margin: const EdgeInsets.all(30),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
      ),
      //width: ,
      height: 250,

      //color: kBackgroundColor,

      // padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      //color: kBackgroundColor,

      child: StreamBuilder<QuerySnapshot>(
        stream: learners_marks,
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("loading");
          }

          final data = snapshot.requireData;
          //print(data.docs[0]['Subjects'].toString());
          //print(data.docs[0]['Subjects'][keys_.keys.elementAt(0)][data
          //     .docs[0]['Subjects'][keys_.keys.elementAt(0)].keys
          //    .elementAt(index)]['Mark']);

          //traverse through each element of list

          return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: DataTable(
                  headingRowColor:
                      MaterialStateColor.resolveWith((states) => kPrimaryColor),
                  columnSpacing: 10,
                  horizontalMargin: 10,
                  columns: [
                    DataColumn(
                        label: Text('Teacher Name',
                            style: TextStyle(
                                fontSize: width_size / 60,
                                // fontFamily: App_FontFamily,
                                color: Colors.white,
                                fontWeight: FontWeight.bold))),
                    DataColumn(
                        label: Text('Teacher_Last_Name',
                            style: TextStyle(
                                fontSize: width_size / 60,
                                // fontFamily: App_FontFamily,
                                color: Colors.white,
                                fontWeight: FontWeight.bold))),
                    /*DataColumn(
                        label: Text('Mark',
                            style: TextStyle(
                                fontSize: width_size / 60,
                                // fontFamily: App_FontFamily,
                                color: Colors.white,
                                fontWeight: FontWeight.bold))),
                    DataColumn(
                        label: Text('Mark Descr',
                            style: TextStyle(
                                fontSize: width_size / 60,
                                //fontFamily: App_FontFamily,
                                color: Colors.white,
                                fontWeight: FontWeight.bold))),
                    DataColumn(
                        label: Text('Date',
                            style: TextStyle(
                                fontSize: width_size / 60,
                                // fontFamily: App_FontFamily,
                                color: Colors.white,
                                fontWeight: FontWeight.bold))),
                    DataColumn(
                        label: Text('Remark',
                            style: TextStyle(
                                fontSize: width_size / 60,
                                // fontFamily: App_FontFamily,
                                color: Colors.white,
                                fontWeight: FontWeight.bold))),*/
                  ],
                  rows: data.docs
                      .map((record) => DataRow(
                              cells: [
                                DataCell(Text(record['Teacher_First_Name'],
                                    style: TextStyle(
                                      fontSize: width_size / 90,
                                      // fontFamily: App_FontFamily,
                                      color: Colors.black,
                                      // fontWeight: FontWeight.bold,
                                    ))),
                                DataCell(Text(record['Teacher_First_Name'],
                                    style: TextStyle(
                                      fontSize: width_size / 90,
                                      //fontFamily: App_FontFamily,
                                      color: Colors.black,
                                      // fontWeight: FontWeight.bold,
                                    ))),
                                /*DataCell(Text(record['Mark'].toString() + "%",
                                    style: TextStyle(
                                      fontSize: width_size / 90,
                                      // fontFamily: App_FontFamily,
                                      color: Colors.black,
                                      // fontWeight: FontWeight.bold,
                                    ))),
                                DataCell(Text(record['Mark_Description'],
                                    style: TextStyle(
                                      fontSize: width_size / 90,
                                      //fontFamily: App_FontFamily,
                                      color: Colors.black,
                                      // fontWeight: FontWeight.bold,
                                    ))),
                                DataCell(Text(record['Datetime'],
                                    style: TextStyle(
                                      fontSize: width_size / 90,
                                      //fontFamily: App_FontFamily,
                                      color: Colors.black,
                                      // fontWeight: FontWeight.bold,
                                    ))),
                                DataCell(Text("Final Mark",
                                    style: TextStyle(
                                      fontSize: width_size / 90,
                                      //fontFamily: App_FontFamily,
                                      color: Colors.greenAccent,
                                      // fontWeight: FontWeight.bold,
                                    ))),*/ //add teacher
                              ],
                              onSelectChanged: (newValue) {
                                print('row 1 pressed');
                              }))
                      .toList()));
        },
      ),
    );
  }
}
