import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

const kPrimaryColor = Colors.blue;
const kPrimaryLightColor = Color.fromARGB(64, 255, 255, 255);
const kBackgroundColor = Color.fromARGB(255, 252, 239, 255);
const kWhite = Colors.white;
const kShadowColor = Colors.black54;
