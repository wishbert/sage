import 'package:flutter/material.dart';
import 'package:sage/Teachers/navigatorBar.dart';
import 'dart:io';

class AppTeacher extends StatefulWidget {
  var teacherId;

  AppTeacher(this.teacherId, {Key? key}) : super(key: key);

  @override
  _AppTeacher createState() => _AppTeacher(teacherId);
}

class _AppTeacher extends State<AppTeacher> {
  var identification;

  _AppTeacher(this.identification);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(''),
      ),
      drawer: NavigatorBar(identification),
    );
  }
}
