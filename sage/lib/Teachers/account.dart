import 'package:flutter/material.dart';
import 'package:sage/constants/colors.dart';
import 'teacherDS.dart';

class Account extends StatefulWidget {
  String ID = '';
  Account(this.ID);
  @override
  _AccountState createState() => _AccountState(ID);
}

class _AccountState extends State<Account> {
  String ID;
  _AccountState(this.ID);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            'My Account',
          ),
        ),
        body: Column(children: [
          const SizedBox(
            height: 80,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: 150,
                height: 150,
                // color: Colors.blue, // can't have both decoration and color
                decoration: const BoxDecoration(
                  color: kPrimaryColor,
                  image: DecorationImage(
                    image: NetworkImage(
                        'https://avatars1.githubusercontent.com/u/49730687?v=4'),
                  ),
                  shape: BoxShape.circle,
                ),
              )
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          DataBase(
            ID,
            'Name',
            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          ),
          const SizedBox(
            height: 20,
          ),
          Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: Container(
                // color: kPrimaryColor,
                height: 200,
                width: double.infinity,
                decoration: BoxDecoration(
                  color: kPrimaryColor,
                  borderRadius: BorderRadius.circular(0),
                  boxShadow: const [
                    BoxShadow(
                      color: kShadowColor,
                      blurRadius: 20.0,
                      // spreadRadius: 20.0,
                    ),
                  ],
                ),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 50,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 50),
                      child: Row(
                        // ignore: prefer_const_literals_to_create_immutables
                        children: [
                          const Icon(
                            Icons.mail,
                            color: kWhite,
                            size: 30.0,
                          ),
                          const SizedBox(
                            width: 20,
                          ),
                          DataBase(ID, 'Email',
                              style: TextStyle(
                                  fontSize: 15, fontWeight: FontWeight.bold))
                        ],
                      ),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(left: 50),
                        child: Row(
                          // ignore: prefer_const_literals_to_create_immutables
                          children: [
                            const Icon(
                              Icons.phone,
                              color: kWhite,
                              size: 30.0,
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Text('0761239876',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold))
                          ],
                        ))
                  ],
                ),
              )),
        ]));
  }
}
