// ignore: file_names

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sage/constants/colors.dart';
import 'account.dart';
import 'summary.dart';
import 'dart:async';
import 'dart:io';
import 'teacherDS.dart';

class NavigatorBar extends StatefulWidget {
  var identification;

  NavigatorBar(this.identification, {Key? key}) : super(key: key);
  @override
  _NavigatorBar createState() => _NavigatorBar(identification);
}

class _NavigatorBar extends State<NavigatorBar> {
  dynamic name = '';
  Text email = Text('');
  String documentId;
  _NavigatorBar(this.documentId);
  @override
  void initState() {
    super.initState();
    // DataBase db = DataBase(documentId);
    setState(() {
      name = DataBase(documentId, 'Name');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            onDetailsPressed: () => {
              Navigator.push(context,
                  MaterialPageRoute(builder: (BuildContext context) {
                return Account(documentId);
              }))
            },
            accountName: DataBase(documentId, 'Name'),
            accountEmail: DataBase(documentId, 'Email'),
            currentAccountPicture: CircleAvatar(
                child: ClipOval(
              child: Image.network(
                  'https://avatars1.githubusercontent.com/u/49730687?v=4',
                  width: 90,
                  height: 90,
                  fit: BoxFit.cover),
            )),
            decoration: const BoxDecoration(
                color: kPrimaryColor,
                image: DecorationImage(
                  image: NetworkImage(
                      'https://cdn.pixabay.com/photo/2018/01/14/23/12/nature-3082832__480.jpg'),
                  fit: BoxFit.cover,
                )),
          ),
          ListTile(
            title: const Text('Overview'),
            onTap: () => null,
          ),
          ListTile(
            title: const Text('Summary'),
            onTap: () => Navigator.push(context,
                MaterialPageRoute(builder: (BuildContext context) {
              return Summary(documentId);
            })),
          ),
          ListTile(
            title: const Text('Calendar'),
            onTap: () => null,
          ),
          ListTile(
            title: const Text('Classes'),
            onTap: () => null,
          ),
          ListTile(
            title: const Text('How To Use'),
            onTap: () => null,
          ),
        ],
      ),
    );
  }
}
