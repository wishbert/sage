import 'package:flutter/material.dart';
import 'package:sage/Teachers/teacherDS.dart';
import 'package:sage/constants/colors.dart';
import 'package:sage/data_manipulation/datatable.dart';
//import 'package:sage/data_manipulation/stylos.dart';

class Summary extends StatefulWidget {
  String ID;
  Summary(this.ID);

  @override
  _SummaryState createState() => _SummaryState(this.ID);
}

class _SummaryState extends State<Summary> with TickerProviderStateMixin {
  String ID;
  _SummaryState(this.ID);
  @override
  Widget build(BuildContext context) {
    TabController _tabController = TabController(length: 2, vsync: this);
    /* return Scaffold(
      appBar: AppBar(title: Text('Summary')),
      body: Container(
        child: Column(
          children: [
            SizedBox(
              height: 20,
            ),
            Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Container(
                  width: double.infinity,
                  height: 110,
                  decoration: BoxDecoration(color: kPrimaryColor, boxShadow: [
                    BoxShadow(color: kShadowColor, blurRadius: 20)
                  ]),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                          color: Colors.pink[900],
                        ),
                      ),
                    ],
                  ),
                )),
            SizedBox(
              height: 20,
            ),
            Flexible(
                flex: 2,
                child: Container(
                  //This is the teacher's recommendations
                  width: double.infinity,
                  height: 165,
                  decoration: BoxDecoration(color: kPrimaryColor, boxShadow: [
                    BoxShadow(color: kShadowColor, blurRadius: 20)
                  ]),
                )),
            SizedBox(
              height: 20,
            ),
            Flexible(
                flex: 3,
                fit: FlexFit.tight,
                child: Container(
                  height: 500,
                  width: double.infinity,
                  decoration: BoxDecoration(color: Colors.red),
                )),
          ],
        ),
      ),
    );
  */

    return Scaffold(
      appBar: AppBar(title: const Text('TEACHER SUMMARY VIEW')),
      body: Column(
        children: [
          //);//scaffold

          Container(
            child: TabBar(
                controller: _tabController,
                labelColor: Colors.black,
                unselectedLabelColor: Colors.grey,
                tabs: [
                  Tab(text: "Grade Taught"),
                  Tab(text: 'Learner'),
                ]),
          ),

          Container(
            width: double.maxFinite,
            height: 300,
            child: TabBarView(
              controller: _tabController,
              children: [
                Data(),
                Text('Learners'),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
