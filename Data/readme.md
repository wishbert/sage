# The Data
- learners_info.csv
- LearnersMarks.csv
- resources.csv
- subjects.csv
- teachers_info.csv
- teacher_subject.csv

## learners_info.csv
This file contains information about the learner.
`L_ID` is a column in the data and it is the unique row data and represents a specific learner.
`profile_photo` is a column in the data and contains directories to the profile photos.
`P_ID` is the unique id of the learner's parent.
The remain columns are self explanitory.

## LearnersMarks.csv
This file contains the subjects a learner has and the tests marks for those subjects.
`LS_ID` is the unique id that identifies a row.
`L_ID` is a unique id that represents a specific learner
`S_ID` is a unique id that represents a specific subject
`Test1-4, Quiz, Exam` are columns that contain the marks a learner got for a specific subject

## resources.csv
This file contains every useful information a student receives from the teacher.
`R_ID` is a unique row identifier
`TS_ID` is a unique identifier that represents the teacher and the subject the teacher is uploading resources to.
`Announcements` This column contains the announcements a teacher makes.
`Notes, Test, Quizzes, Exam` this columns contain directories to where the said resources are stored.

## subjects.csv
This file contains all the subjects being tought at a school along with their unique ids.
`S_ID` is the unique subject identifier

## teachers_info.csv
This file contains information about the teachers.
`T_ID` is a unique id that represents a specific teacher.
`offices` this column contains the teacher's office number, which is optional.
`profile_photo` this column contains directories to the profile photos of the teachers.

## teacher_subject.csv
This file contains subjects and teachers who teach those subjects for specific grades/classes.
The data is in the form of ids.